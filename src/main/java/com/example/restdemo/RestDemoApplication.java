package com.example.restdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static java.time.LocalDateTime.now;

@SpringBootApplication
@RestController
public class RestDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestDemoApplication.class, args);
    }


    @GetMapping("/greetingvikram")
    public String greeting() {
        return "Hello Vickram this is new jar pls" + now();
    }

    @GetMapping("/")
    public String welcome() {
        return "Welcome page" + now();
    }
}
