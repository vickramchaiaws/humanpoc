$foundProcesses = netstat -ano | findstr :8081
$activePortPattern = ":8081\s.+LISTENING\s+\d+$"
$pidNumberPattern = "\d+$"

gsutil cp gs://ah-ust-ustil-compute-np-870-local-cicd-np-dev/temp/rest-demo-0.0.1-SNAPSHOT.jar C:\Demo\download\
if ($foundProcesses | Select-String -Pattern $activePortPattern -Quiet) {
  $matches = $foundProcesses | Select-String -Pattern $activePortPattern
  $firstMatch = $matches.Matches.Get(0).Value

  $pidNumber = [regex]::match($firstMatch, $pidNumberPattern).Value
  $hashvalueold=(Get-FileHash C:\Demo\deployment\rest-demo-0.0.1-SNAPSHOT.jar -Algorithm MD5).Hash | select -first 1
  $hashvaluenew=(Get-FileHash C:\Demo\download\rest-demo-0.0.1-SNAPSHOT.jar -Algorithm MD5).Hash | select -first 1
  Write-Output $hashvalueold
  Write-Output $hashvaluenew
  if ($hashvalueold -ceq $hashvaluenew ) {
	Write-Output "both are same"
	}else{ Write-Output "both are different"
	taskkill /pid $pidNumber /f
	Move-item -path "C:\Demo\download\rest-demo-0.0.1-SNAPSHOT.jar" -destination "C:\Demo\deployment" -force
	Start-Process -FilePath java -ArgumentList '-jar C:\Demo\deployment\rest-demo-0.0.1-SNAPSHOT.jar'	
  }	
}else {
Write-Output "instance is not running please start it"
Move-item -path "C:\Demo\download\rest-demo-0.0.1-SNAPSHOT.jar" -destination "C:\Demo\deployment" -force
Start-Process -FilePath java -ArgumentList '-jar C:\Demo\deployment\rest-demo-0.0.1-SNAPSHOT.jar'
}
