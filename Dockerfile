FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
COPY test.sh /usr/data/hpscil/certificates/postgresql/
COPY test.sh /usr/data/hpscil/certificates/postgresql-2/ 
COPY test.sh /usr/data/hpscil/integration/hrp/realtime/
RUN chmod -R 777 /usr/
EXPOSE 8081
ENTRYPOINT ["java","-jar","/app.jar"]
EXPOSE 8081 8081
