



$foundProcesses = netstat -ano | findstr :8081
$activePortPattern = ":8081\s.+LISTENING\s+\d+$"
$pidNumberPattern = "\d+$"

IF ($foundProcesses | Select-String -Pattern $activePortPattern -Quiet) {
  $matches = $foundProcesses | Select-String -Pattern $activePortPattern
  $firstMatch = $matches.Matches.Get(0).Value

  $pidNumber = [regex]::match($firstMatch, $pidNumberPattern).Value

  taskkill /pid $pidNumber /f
}

Start-Process -FilePath java -ArgumentList '-jar C:\hrp\rest-demo-0.0.1-SNAPSHOT.jar'

$action = New-ScheduledTaskAction -Execute C:\hrp\killPort.ps1
$trigger = New-ScheduledTaskTrigger -Daily -At 4am
$task = Register-ScheduledTask -TaskName "MyTask" -Trigger $trigger -Action $action
$task.Triggers.Repetition.Duration = "P1D" 
$task.Triggers.Repetition.Interval = "PT10M" 
$task | Set-ScheduledTask

$trigger = New-JobTrigger -AtStartup -RandomDelay 00:00:30
Register-ScheduledJob -Trigger $trigger -FilePath C:\hrp\killPort.ps1 -Name GetBatteryStatus
