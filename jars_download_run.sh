start_application(){
        cd /home/vickram/humana/"$3"
        echo "starting the application"
        nohup java -jar $1 >/dev/null 2>&1 &
        sleep 50
        health=`curl -sL -w "%{http_code}\\n" "http://34.71.115.51:8081/greeting" -o /dev/null`
        if [ "$health" -eq 200 ]
        then
                echo "application started suscessfully"
                echo "$2"
                if [ ! -z "$2" ]
                then
                        mv /home/vickram/humana/"$3"/"$2" /home/vickram/oldJars/
                        gsutil -q stat gs://latest-jars/"$2"
                        /snap/bin/gsutil mv gs://latest-jars/"$2" gs://archive_jars
                fi
        else
           echo "application did not start restart it with old jar"
           nohup java -jar "$2" >/dev/null 2>&1 &
           mv /home/vickram/humana/"$3"/"$1" /home/vickram/oldJars/
           /snap/bin/gsutil mv gs://latest-jars/"$1" gs://archive_jars
           echo "hi"
        fi
}

echo "saving the old jar to variable"
#rm -rf /home/vickram/jars/*
application="$1"
snapshot=${application%-*}
version=${snapshot%-*}
service=${version%-*}
oldJar=""
/snap/bin/gcloud auth activate-service-account --key-file /home/vickram/sdk/account.json
/snap/bin/gsutil cp gs://latest-jars/$1 /home/vickram/jars
if [ -d /home/vickram/humana/$service ]
then
        echo "directory exist"
        oldJar=`ls /home/vickram/humana/"$service"`
        mv /home/vickram/jars/"$application" /home/vickram/humana/$service
else
        echo "directory does not exit pls create it"
        mkdir -p /home/vickram/humana/$service
        mv /home/vickram/jars/"$application" /home/vickram/humana/$service
fi
count=`ps -eaf | grep -v grep | grep "$service" | grep -v sh | wc -l`
if [ "$count" -gt 0 ]
then
echo " Application is Running stop and start it"
pid=`ps -eaf | grep -v grep | grep "$service" | grep -v sh | awk '{print $2}'`
kill "$pid"
sleep 20
count=`ps -eaf | grep -v grep | grep "$service" | grep -v sh | wc -l`
        if [ "$count" -gt 0 ]
        then
                echo "Application is still running forcefully stop"
                kill -9 "$pid"
        else
                echo "Application is already stopped"
        fi
start_application $1 "$oldJar" "$service"
else
echo "Application is not Running you can start the application"
start_application $1 "$oldJar" "$service"
fi