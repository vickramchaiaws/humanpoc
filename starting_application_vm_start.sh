
start_application() {
cd /home/vickram/humana/$2
echo "starting the application"
nohup java -jar $1 >/dev/null 2>&1 &
sleep 50
health=`curl -sL -w "%{http_code}\\n" "http://34.72.131.86:8081/greeting" -o /dev/null`
if [ "$health" -eq 200 ]
then
        echo "application started suscessfully"
else
        echo "application did not start please check"
fi
}

/snap/bin/gcloud auth activate-service-account --key-file /home/vickram/sdk/account.json
/snap/bin/gsutil cp gs://latest-jars/* /home/vickram/jars
for i in `ls /home/vickram/jars/`
do
        echo "$i"
        application="$i"
        snapshot=${application%-*}
        version=${snapshot%-*}
        service=${version%-*}
        if [ -d /home/vickram/humana/$service ]
        then
                echo "directory exist"
                mv /home/vickram/jars/"$application" /home/vickram/humana/$service
        else
                echo "directory does not exit pls create it"
                mkdir -p /home/vickram/humana/$service
                mv /home/vickram/jars/"$application" /home/vickram/humana/$service
        fi
        start_application "$application" "$service"
done